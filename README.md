# Twitter API #

Dieses Projekt ist zur Abfrage der Twitter API und zum bereitstellen einer eigenen API um die Ergebnisse von Twitter auswerten zu können.

### Build

Als Build-Management-System wird Maven verwendet. Das komplette Projekt kann mittels

> mvn clean install

gebaut werden. Hierbei werden keine Komponenten/Module auf dem Nexus deployed. Es
wird nur eine lokale Installation in das lokale Maven-Repository durchgeführt.

Die Installation der POM ohne die Unterprojekte kann mittels

> mvn clean install -N

durchgeführt werden.

Nach der Installation kann unter /target das Programm gestartet werden

java -jar twitter-test-0.1.0-SNAPSHOT.jar

### Usage ###

Die Rest-API ist erreichbar unter:
http://localhost:8080

Für die Anzahl der Tweets pro Stunde für den heutigen Tag:
/twitter/tweets/number

Für die Anzahl der User die Über das Thema getweetet haben
/twitter/tweets/user/number

### Frameworks

Folgende Frameworks werden verwendet:

- Spring
- Lombok
- Swagger
- Twitter4j