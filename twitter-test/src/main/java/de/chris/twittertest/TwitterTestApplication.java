package de.chris.twittertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * Allergen- Web Application.
 *
 * @author Chris
 */
@ComponentScan("de.chris")
@SpringBootApplication
public class TwitterTestApplication {

  /**
   * Main.
   *
   * @param args Argumente
   */
  public static void main(String[] args) {

    SpringApplication.run(TwitterTestApplication.class, args);
  }
}
