package de.chris.twittertest.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;

/**
 * Konfiguration für Twitter.
 *
 * @author Chris
 */
@Slf4j
@Configuration
public class TwitterConfig {

  /**
   * {@link Bean} für {@link Twitter} abfragen.
   */
  @Bean
  public Twitter twitter() {

    Twitter twitter = TwitterFactory.getSingleton();

    return twitter;
  }
}
