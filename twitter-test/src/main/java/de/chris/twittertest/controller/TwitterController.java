package de.chris.twittertest.controller;

import de.chris.twittertest.core.HashTag;
import de.chris.twittertest.core.PostDay;
import de.chris.twittertest.service.SocialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller für die Twitter Abfragen.
 *
 * @author chris
 */
@AllArgsConstructor
@RestController
@RequestMapping(value = "/twitter",
    produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "Twitter", value = " ")
public class TwitterController {

  /**
   * {@link SocialService}.
   */
  private SocialService socialService;

  /**
   * Gibt die Anzahl der Tweets zurück.
   *
   * @return {@link Integer}
   */
  @ApiOperation("Returns the number of Tweets.")
  @GetMapping(value = "/tweets/number")
  public Integer getNumberOfTweets() {

    return this.socialService.getPostCount(HashTag.valueOf("IoT")).getValuePerDayHour();
  }

  /**
   * Gibt die Anzahl der User eines Tweets.
   *
   * @return {@link Integer}
   */
  @ApiOperation("Returns the number of Tweet User.")
  @GetMapping(value = "/tweets/user/number")
  public Integer getNumberOfUser() {

    HashTag hashTag = new HashTag("IoT", PostDay.today());

    return this.socialService.getUserCount(hashTag).getValue();
  }

}
