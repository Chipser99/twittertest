package de.chris.twittertest.core;

import java.time.LocalDateTime;
import lombok.Value;

/**
 * Valueobject für das Zählen.
 *
 * @author Chris
 */
@Value
public class Count {

  /**
   * value.
   */
  private int value;

  /**
   * Konstruktor bei dem Überprüft wird ob der Value größer gleich 0.
   *
   * @param value value
   */
  public Count(int value) {

    if (value < 0) {
      throw new IllegalArgumentException("Value of count have to be greater or equals zero.");
    }

    this.value = value;
  }

  /**
   * Value pro Stunde vom laufenden Tag zurückliefern.
   *
   * @return int
   */
  public int getValuePerDayHour() {

    return value / LocalDateTime.now().getHour();
  }

  /**
   * Value pro Stunde zurückliefern.
   *
   * @return int
   */
  public int getValuePerHour() {

    return value / 24;
  }

}
