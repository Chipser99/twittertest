package de.chris.twittertest.core;

import lombok.NonNull;
import lombok.Value;
import org.springframework.util.StringUtils;

/**
 * Valueobject für die Suche nach einem Hashtag.
 *
 * @author Chris
 */
@Value
public class HashTag {

  /**
   * value.
   */
  private final String value;

  /**
   * {@link PostDay}.
   */
  private final PostDay day;

  /**
   * Konstruktor wo überprüft wird ob value leer.
   *
   * @param value value
   */
  public HashTag(String value, @NonNull PostDay day) {

    if (StringUtils.isEmpty(value)) {
      throw new IllegalArgumentException("The value of hashtag cannot be null or empty.");
    }

    this.value = value;
    this.day = day;
  }

  /**
   * Initialisiert neuen Hashtag mit Value.
   *
   * @param value value
   *
   * @return {@link HashTag}
   */
  public static HashTag valueOf(String value) {

    return new HashTag(value, PostDay.today());
  }

  /**
   * Value ausgeben mit Hashtag.
   *
   * @return value
   */
  public String getTagValue() {

    return "#" + value;
  }

  @Override
  public String toString() {

    return getTagValue();
  }
}


