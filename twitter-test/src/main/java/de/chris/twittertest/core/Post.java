package de.chris.twittertest.core;

import lombok.Value;
import org.springframework.util.StringUtils;

/**
 * Valueobject für Posts.
 *
 * @author Chris
 */
@Value
public class Post {

  /**
   * Postmessage.
   */
  private final String message;

  /**
   * User.
   */
  private final User user;

  /**
   * Konstruktor.
   *
   * @param user {@link User}
   * @param message message
   */
  public Post(User user, String message) {

    if (StringUtils.isEmpty(message)) {
      throw new IllegalArgumentException("The value of message cannot be null or empty.");
    }

    this.user = user;
    this.message = message;

  }
}

