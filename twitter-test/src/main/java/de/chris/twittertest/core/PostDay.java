package de.chris.twittertest.core;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;
import lombok.NonNull;
import lombok.Value;

@Value
public class PostDay {

  private static final String DELIMITER = "-";
  private static final Pattern YEAR_PATTERN = Pattern.compile("([1-2][0-9]{3})");
  private static final Pattern MONTH_PATTERN = Pattern.compile("[0-9]{2}");
  private static final Pattern DAY_PATTERN = Pattern.compile("[0-9]{2}");
  private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  private final String until;

  private final String since;

  private PostDay(@NonNull String year, @NonNull String month, @NonNull String date) {

    if (isNotValid(year, YEAR_PATTERN)) {
      throw new IllegalArgumentException("The value of year does not represent the format (YYYY).");
    }

    if (isNotValid(month, MONTH_PATTERN)) {
      throw new IllegalArgumentException("The value of month does not represent the format (MM).");
    }

    if (isNotValid(date, DAY_PATTERN)) {
      throw new IllegalArgumentException("The value of date does not represent the format (DD).");
    }

    this.since = year + DELIMITER + month + DELIMITER + date + DELIMITER;
    this.until = generateTomorrow(this.since);
  }

  private String generateTomorrow(@NonNull String day) {
    LocalDate dateTime = LocalDate.parse(day);
    LocalDate tomorrow = dateTime.plusDays(1);

    return tomorrow.format(formatter);
  }

  private PostDay(@NonNull String since) {

    this.since = since;
    this.until = generateTomorrow(this.since);
  }

  private static boolean isNotValid(String value, Pattern pattern) {

    return !pattern.matcher(value).matches();
  }

  public static PostDay today() {

    LocalDate now = LocalDate.now();

    String formatDateTime = now.format(formatter);

    return new PostDay(formatDateTime);
  }

  public static PostDay valueOf(@NonNull String year, @NonNull String month, @NonNull String date) {

    return new PostDay(year, month, date);
  }
}
