package de.chris.twittertest.core;

import lombok.Value;
import org.springframework.util.StringUtils;

/**
 * Valueobject für User.
 *
 * @author Chris
 */
@Value
public class User {

  /**
   * Name des Users.
   */
  public String name;

  /**
   * Konstruktor mit emty Prüfung.
   *
   * @param name Name
   */
  public User(String name) {

    if (StringUtils.isEmpty(name)) {
      throw new IllegalArgumentException("The value of name cannot be null or empty.");
    }

    this.name = name;
  }

}
