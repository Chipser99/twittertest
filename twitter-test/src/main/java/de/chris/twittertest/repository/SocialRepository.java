package de.chris.twittertest.repository;

import de.chris.twittertest.core.Count;
import de.chris.twittertest.core.HashTag;
import java.util.List;

/**
 * {@link org.springframework.stereotype.Repository} für die Abfrage Sozialer Medien.
 *
 * @param <T> Aufbau des zu suchenden Objectes
 *
 * @author Chris
 */
public interface SocialRepository<T> {

  /**
   * Alle Posts nach einem Hashtag suchen.
   *
   * @param hashTag {@link HashTag}
   *
   * @return {@link List} von T
   */
  List<T> findAll(HashTag hashTag);

  /**
   * Zählen von Posts mit einem Hashtag.
   *
   * @param hashTag {@link HashTag}
   *
   * @return {@link Count}
   */
  Count count(HashTag hashTag);

}
