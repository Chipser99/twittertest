package de.chris.twittertest.repository;

import de.chris.twittertest.core.Count;
import de.chris.twittertest.core.HashTag;
import de.chris.twittertest.core.Post;
import de.chris.twittertest.core.User;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * {@link Repository} für Twitter.
 *
 * @author Chris
 */
@Slf4j
@Repository
public class TwitterSocialRepository implements SocialRepository<Post> {

  /**
   * {@link Twitter} Bean.
   */
  @Autowired
  private Twitter twitter;

  @Override
  public List<Post> findAll(HashTag hashTag) {

    List<Post> result = new ArrayList<>();

    Query query = buildQuery(hashTag);
    QueryResult queryResult;

    try {
      queryResult = twitter.search(query);
      List<Status> statuses = queryResult.getTweets();

      result.addAll(statuses.stream().map(this::createPost)
          .collect(Collectors.toList()));

    } catch (TwitterException e) {
      log.warn("Twitter konnte die Anfrage nicht korrekt verwerten.");
      e.printStackTrace(); // ToDo: logging
      return result;
    }

    return result;
  }

  /**
   * Verarbeiten von {@link Status} zu {@link Post}.
   *
   * @param status {@link Status}
   * @return {@link Post}
   */
  private Post createPost(Status status) {
    User user = new User(status.getUser().getScreenName());
    return new Post(user, status.getText());
  }

  /**
   * {@link Query} für Abfrage erzeugen.
   *
   * @param hashTag {@link HashTag}
   * @return {@link Query}
   */
  private Query buildQuery(@NonNull HashTag hashTag) {

    Query query = new Query(hashTag.getTagValue());
    query.setSince(hashTag.getDay().getSince());
    query.setUntil(hashTag.getDay().getUntil());
    return query;
  }

  @Override
  public Count count(@NonNull HashTag hashTag) {

    return new Count(findAll(hashTag).size());
  }
}
