package de.chris.twittertest.service;

import de.chris.twittertest.core.Count;
import de.chris.twittertest.core.HashTag;
import lombok.NonNull;

/**
 * Social Service.
 *
 * @author Chris
 */
public interface SocialService {

  /**
   * Count User.
   *
   * @return {@link Count}
   */
  Count getUserCount(@NonNull HashTag hashTag);

  /**
   * Count Posts.
   *
   * @return {@link Count}
   */
  Count getPostCount(@NonNull HashTag hashTag);

}
