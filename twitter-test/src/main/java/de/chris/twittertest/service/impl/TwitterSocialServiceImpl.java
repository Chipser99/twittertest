package de.chris.twittertest.service.impl;

import de.chris.twittertest.core.Count;
import de.chris.twittertest.core.HashTag;
import de.chris.twittertest.core.Post;
import de.chris.twittertest.core.User;
import de.chris.twittertest.repository.SocialRepository;
import de.chris.twittertest.service.SocialService;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Service;

/**
 * Social Service für Twitter.
 *
 * @author Chris
 */
@AllArgsConstructor
@Service
public class TwitterSocialServiceImpl implements SocialService {

  /**
   * {@link SocialRepository}.
   */
  private SocialRepository<Post> socialRepository;

  @Override
  public Count getUserCount(@NonNull HashTag hashTag) {

    List<Post> posts = socialRepository.findAll(hashTag);

    Map<User, List<Post>> collect = posts.stream().collect(Collectors.groupingBy(Post::getUser));

    return new Count(collect.size());

  }

  @Override
  public Count getPostCount(@NonNull HashTag hashTag) {

    return socialRepository.count(hashTag);
  }
}
