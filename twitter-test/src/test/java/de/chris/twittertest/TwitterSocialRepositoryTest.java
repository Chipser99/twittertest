package de.chris.twittertest;

import de.chris.twittertest.core.HashTag;
import de.chris.twittertest.core.Post;
import de.chris.twittertest.core.PostDay;
import de.chris.twittertest.repository.TwitterSocialRepository;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test der Verbindung zu Twitter.
 *
 * @author chris
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TwitterTestApplication.class)
public class TwitterSocialRepositoryTest {

  /**
   * {@link TwitterSocialRepository}.
   */
  @Autowired
  private TwitterSocialRepository socialRepository;

  /**
   * Test ob die Twitter API Objekte zurückliefert.
   */
  @Test
  public void testFindAll() {

    HashTag hashTag = new HashTag("IoT", PostDay.today());

    List<Post> postList = socialRepository.findAll(hashTag);

    Assert.assertNotNull(postList);

  }

}
