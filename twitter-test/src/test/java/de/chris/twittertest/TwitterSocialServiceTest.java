package de.chris.twittertest;

import de.chris.twittertest.core.Count;
import de.chris.twittertest.core.HashTag;
import de.chris.twittertest.core.Post;
import de.chris.twittertest.core.PostDay;
import de.chris.twittertest.repository.TwitterSocialRepository;
import de.chris.twittertest.service.impl.TwitterSocialServiceImpl;
import de.chris.twittertest.support.TwitterUtils;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test für {@link TwitterSocialServiceImpl}.
 *
 * @author chris
 */
@SpringBootTest(classes = TwitterTestApplication.class)
@RunWith(SpringRunner.class)
public class TwitterSocialServiceTest {

  /**
   * {@link TwitterSocialRepository} gemocked.
   */
  @MockBean
  @Autowired
  private TwitterSocialRepository socialRepository;

  /**
   * {@link TwitterSocialServiceImpl}.
   */
  @Autowired
  private TwitterSocialServiceImpl socialService;

  /**
   * Test der PostCount Methode.
   */
  @Test
  public void testPostCount() {

    HashTag hashTag = new HashTag("IoT", PostDay.today());

    List<Post> mockedPostList = TwitterUtils.createPostList();

    Mockito.when(this.socialRepository.count(hashTag)).thenReturn(new Count(mockedPostList.size()));

    Count count = socialService.getPostCount(hashTag);

    Assert.assertEquals(mockedPostList.size(), count.getValue());

  }

  /**
   * Test der UserCount Methode.
   */
  @Test
  public void testUserCount() {

    HashTag hashTag = new HashTag("IoT", PostDay.today());

    List<Post> mockedPostList = TwitterUtils.createPostList();

    Mockito.when(this.socialRepository.findAll(hashTag)).thenReturn(mockedPostList);

    Count count = socialService.getUserCount(hashTag);

    Assert.assertEquals(mockedPostList.size(), count.getValue());

  }

}
