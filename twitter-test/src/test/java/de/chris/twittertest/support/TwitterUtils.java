package de.chris.twittertest.support;

import de.chris.twittertest.core.Post;
import de.chris.twittertest.core.User;
import java.util.ArrayList;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utility-Klasse für die Twitter Valueobjects
 *
 * @author chris
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TwitterUtils {

  /**
   * Erstellt eine {@link List} für {@link Post}
   *
   * @return {@link List} für {@link Post}
   */
  public static List<Post> createPostList() {

    User user = new User("TestUser");
    Post post = new Post(user, "message");
    List<Post> postList = new ArrayList<>();
    postList.add(post);

    return postList;
  }
}
